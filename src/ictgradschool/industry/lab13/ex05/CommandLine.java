package ictgradschool.industry.lab13.ex05;

import ictgradschool.Keyboard;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

/**
 * Created by ljam763 on 1/05/2017.
 */
public class CommandLine{
    private long number;
    public CommandLine(){
        this.number = numberInput();
    }
    public long getInputNumber() {
        return inputNumber;
    }

    public void setInputNumber(long inputNumber) {
        this.inputNumber = inputNumber;
    }

    private long inputNumber;
    public long numberInput(){
        inputNumber = Long.parseLong(Keyboard.readInput());
        return inputNumber;
    }

    public static void main(String[] args) {
        final String[] inputKey = new String[1];
        System.out.println("Please enter a number");
        CommandLine thing = new CommandLine();
        PrimeFactorsTask asd = new PrimeFactorsTask(thing.getInputNumber());
        Thread consumer1 = new Thread(asd);
        Thread termination = new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println("Type and enter to stop");
                String thing = Keyboard.readInput();
                if ( thing!= ""){
                    System.out.println("Stop");
                    consumer1.stop();
                }
            }
        });
        consumer1.start();
        termination.start();
        try{
            System.out.println("The end");
            consumer1.join();
            System.out.println(asd.getPrimeFactors());
            System.out.println(asd.getState());
        }
        catch (InterruptedException e){
            e.getMessage();
        }
        catch (IllegalStateException e){
            e.getMessage();
        }
        
    }

}
