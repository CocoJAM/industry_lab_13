package ictgradschool.industry.lab13.ex05;

import ictgradschool.industry.lab13.examples.example03.PrimeFactors;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ljam763 on 1/05/2017.
 */
public class PrimeFactorsTask implements Runnable {
    private long N;
    private List<Long> listOfPrimeFactors = new ArrayList<>();
    private TaskState state = TaskState.Initialized;

    public enum TaskState {Initialized, Completed, Aborted}

    public PrimeFactorsTask(long n) {
        this.N = n;
    }

    @Override
    public void run() {
        for (long factor = 2; factor * factor <= N && !Thread.currentThread().isInterrupted(); factor++) {
            // if factor is a factor of n, repeatedly divide it out
            while (N % factor == 0) {
                listOfPrimeFactors.add(factor);
                N = N / factor;
            }

        }
        state = TaskState.Completed;
    }

    public long n() {
        return listOfPrimeFactors.get(listOfPrimeFactors.size() - 1);
    }

    public List<Long> getPrimeFactors() throws IllegalStateException {
        while (state.equals(TaskState.Completed)) {
            return listOfPrimeFactors;
        }
        throw new IllegalStateException();
    }

    public TaskState getState() {
        return state;
    }
}
