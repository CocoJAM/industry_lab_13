package ictgradschool.industry.lab13.ex04;

import java.util.List;
import java.util.concurrent.BlockingQueue;

/**
 * Created by ljam763 on 1/05/2017.
 */
public class Consumer implements Runnable {
    private BlockingQueue queue;
    private BankAccount account;
    List<Transaction> transactions;

    public Consumer(BlockingQueue queue, BankAccount account, List<Transaction> transactions) {
        this.queue = queue;
        this.account = account;
        this.transactions = transactions;
    }

    @Override
    public void run() {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                transectionSwitch((Transaction) queue.take(), account);
            } catch (InterruptedException e) {
                System.out.println("Hello there");
              e.getStackTrace();
               // e.getMessage();
            }
        }
    }

    public void transectionSwitch(Transaction thing, BankAccount account) {
        switch (thing._type) {
            case Deposit:
                account.deposit(thing._amountInCents);
                break;
            case Withdraw:
                account.withdraw(thing._amountInCents);
                break;
        }
    }
}
