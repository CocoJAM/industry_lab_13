package ictgradschool.industry.lab13.ex04;

import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

/**
 * Created by ljam763 on 1/05/2017.
 */
public class ConcurrentBankingApp {
    public static void main(String[] args) {
        List<Transaction> transactions = TransactionGenerator.readDataFile();
        final BlockingQueue<Transaction> queue = new ArrayBlockingQueue<>(10);

        // Create BankAccount object to operate on.
        BankAccount account = new BankAccount();

        Thread producer = new Thread(new Runnable() {
            @Override
            public void run() {
                for (Transaction transaction : transactions) {
                    try {
                        queue.put(transaction);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
        producer.start();
        Thread consumer1 = new Thread(new Consumer(queue,account,transactions), "User 1");
        consumer1.start();
        Thread consumer2 = new Thread(new Consumer(queue,account,transactions),"User 2");
        consumer2.start();
        try {
            producer.join();
            consumer1.interrupt();
            consumer2.interrupt();
        }
        catch (InterruptedException e){
            e.getCause();
        }
        // Print the final balance after applying all Transactions.
        System.out.println("Final balance: " + account.getFormattedBalance());
    }

}
