package ictgradschool.industry.lab13.ex03;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Created by ljam763 on 1/05/2017.
 */
public class ExerciseThree100Thread extends ExerciseThreeSingleThreaded{
    private long numInsideCircle = 0;
    private double x;
    private double y;
    private double estimatedPi;

    protected double estimatePI(long numSamples) {
        List<Thread> listOfThread = new ArrayList<>();
        for (int i = 0; i < 100; i++) {
            Thread t = new Thread(new ExrcisThreeRunnable() {
                ThreadLocalRandom random =ThreadLocalRandom.current();
                @Override
                public void run() {
                    for (int i = 0; i < numSamples/100; i++) {
                        double x = random.nextDouble();
                        double y = random.nextDouble();
                        if (Math.pow(x, 2.0) + Math.pow(y, 2.0) < 1.0) {
                            numInsideCircle++;
                        }}
                }
            });
            t.start();
            listOfThread.add(t);
            try {
                for (Thread thread : listOfThread) {
                    thread.join();
                }
            } catch (InterruptedException e) {
                e.getCause();
            }}

        estimatedPi = 4.0 * (double) numInsideCircle / (double) numSamples;
        return estimatedPi;
    }
    public static void main(String[] args) {
        new ExerciseThree100Thread().start();
    }
}
