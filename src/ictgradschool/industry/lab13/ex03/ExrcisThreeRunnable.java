package ictgradschool.industry.lab13.ex03;

/**
 * Created by ljam763 on 1/05/2017.
 */
public class ExrcisThreeRunnable implements Runnable{
    private long numInsideCircle =0 ;
    @Override
    public void run() {
            double x = Math.random();
            double y = Math.random();
            if (Math.pow(x, 2.0) + Math.pow(y, 2.0) < 1.0) {
                numInsideCircle++;
            }
    }
}
