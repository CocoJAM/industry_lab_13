package ictgradschool.industry.lab13.ex03;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Created by ljam763 on 1/05/2017.
 */
public class ExerciseThree2Thread extends ExerciseThreeSingleThreaded {

    private long numInsideCircle = 0;
    private double x;
    private double y;
    private Thread t1 = new Thread(new ExrcisThreeRunnable() {
        @Override
        public void run() {
            ThreadLocalRandom random =ThreadLocalRandom.current();
            double x = random.nextDouble();
            double y = random.nextDouble();
            if (Math.pow(x, 2.0) + Math.pow(y, 2.0) < 1.0) {
                numInsideCircle++;
            }
        }
    });
    private Thread t2= new Thread(new Runnable() {
        @Override
        public void run() {
            double x = Math.random();
            double y = Math.random();
            if (Math.pow(x, 2.0) + Math.pow(y, 2.0) < 1.0) {
                numInsideCircle++;
            }
        }
    });
    private double estimatedPi;

    @Override
    protected double estimatePI(long numSamples) {
        List<Thread> listOfThread = new ArrayList<>();
        for (int i = 0; i < numSamples/2; i++) {
            try {
                if (!t1.isAlive()){
                t1.run();}
                if (!t2.isAlive()){
                t2.run();}
                t1.join();
                t2.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        estimatedPi = 4.0 * (double) numInsideCircle / (double) numSamples;
        return estimatedPi;
    }
    public static void main(String[] args) {
        new ExerciseThree2Thread().start();
    }
}
