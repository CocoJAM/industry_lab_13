package ictgradschool.industry.lab13.ex03;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

/**
 * TODO Create a multi-threaded program to calculate PI using the Monte Carlo method.
 */
public class ExerciseThreeMultiThreaded extends ExerciseThreeSingleThreaded {
    private long numInsideCircle = 0;
    /**
     * Estimates PI using a multi-threaded Monte Carlo method.
     */
    @Override
    protected double estimatePI(long numSamples) {
        // TODO Implement this.
        List<Thread> listOfThread = new ArrayList<>();
        double estimatedPi;

        for (int i = 0; i < numSamples; i++) {
            Thread t = new Thread(new Runnable() {
                @Override
                public void run() {
                    ThreadLocalRandom random =ThreadLocalRandom.current();
                    double x = random.nextDouble();
                    double y = random.nextDouble();
                    if (Math.pow(x, 2.0) + Math.pow(y, 2.0) < 1.0) {
                        numInsideCircle++;
                    }
                }
            });
            t.start();
            listOfThread.add(t);
        }
        try {
            for (Thread thread : listOfThread) {
                thread.join();
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        estimatedPi = 4.0 * (double) numInsideCircle / (double) numSamples;
        return estimatedPi;
    }

    /**
     * Program entry point.
     */
    public static void main(String[] args) {
        new ExerciseThreeMultiThreaded().start();
    }
}
